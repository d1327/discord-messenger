﻿namespace DiscordMessegeSender
{
    public interface IDiscordMessenger
    {
        public void SendMessage(string message, string botId, long channelId);
    }
}