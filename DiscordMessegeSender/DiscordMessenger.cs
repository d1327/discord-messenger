﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using DiscordMessegeSender.Consts;

namespace DiscordMessegeSender
{
    public class DiscordMessenger : IDiscordMessenger
    {
        public void SendMessage(string message, string botId, long channelId)
        {
            var msg = new DiscordMessage { Content = message };
            var url = $"{Urls.ChannelUrl}/{channelId}/{Urls.MessagePath}";

            using var client = new HttpClient();
            client.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bot {botId}");

            client.PostAsJsonAsync(url, msg).GetAwaiter().GetResult();
        }
    }
}