﻿namespace DiscordMessegeSender.Consts
{
    internal class Urls
    {
        internal static readonly string BaseUrl = "https://discordapp.com/api";

        internal static readonly string ChannelUrl = $"{BaseUrl}/channels";

        internal static readonly string MessagePath = $"messages";
    }
}